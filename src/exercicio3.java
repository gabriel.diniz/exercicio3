import java.util.Scanner;

public class exercicio3 {

    public static void main(String[] args) { // metodo publico estatico

        Scanner in = new Scanner(System.in);

        int saldo; // variavel

        System.out.println("Digite o valor para reajuste: "); // inserir valor
        saldo = in.nextInt();

        System.out.println("O valor com reajuste é: "+ (saldo*1.01)); // imprimir valor com reajuste * 1.01
    }
}